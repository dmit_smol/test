from django.db import models

# Create your models here.


class Feedback(models.Model):
    theme = models.CharField(max_length=500)
    description = models.CharField(max_length=2500)
    created_at = models.DateTimeField(auto_now_add=True)
    email = models.EmailField()

    class Meta:
        ordering = ['-created_at']

    def __str__(self):

        return self.theme
